package com.soulpatch.mytodo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.soulpatch.mytodo.ui.fragments.MainFragment;
import com.soulpatch.mytodo.ui.fragments.PendingTodoListFragment;
import com.soulpatch.mytodo.ui.fragments.SettingsFragment;
import com.soulpatch.mytodo.ui.fragments.StatisticsFragment;
import com.soulpatch.mytodo.utils.Utils;

/**
 * Starting Activity of the app
 *
 * @author Akshay Viswanathan
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout mDrawerLayout;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.nav_drawer_open, R.string.nav_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(getString(R.string.app_name));
        }

        final FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
        fragTrans.replace(R.id.container, new MainFragment(), Utils.TAG).commit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBackPressed() {
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        final FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        final int id = item.getItemId();
        final FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
        mDrawerLayout.closeDrawers();
        switch (id) {
            case R.id.todo_list:
                fragTrans.replace(R.id.container, new MainFragment(), MainActivity.class.getSimpleName());
                fragTrans.addToBackStack(MainFragment.class.getSimpleName()).commit();
                return true;
            case R.id.pending_todos:
                fragTrans.replace(R.id.container, new PendingTodoListFragment(), MainActivity.class.getSimpleName());
                fragTrans.addToBackStack(PendingTodoListFragment.class.getSimpleName()).commit();
                return true;
            case R.id.ic_settings:
                fragTrans.replace(R.id.container, new SettingsFragment(), MainActivity.class.getSimpleName());
                fragTrans.addToBackStack(SettingsFragment.class.getSimpleName()).commit();
                return true;
            case R.id.ic_statistics:
                fragTrans.replace(R.id.container, new StatisticsFragment(), MainActivity.class.getSimpleName());
                fragTrans.addToBackStack(StatisticsFragment.class.getSimpleName()).commit();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
