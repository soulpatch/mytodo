package com.soulpatch.mytodo.database;

import android.provider.BaseColumns;

/**
 * Database contract for the application
 *
 * @author Akshay Viswanathan
 */
public class MyTodoDBContract {
    private MyTodoDBContract() {
    }

    public static class TODOListEntry implements BaseColumns {
        static final String TABLE_NAME = "TODO_LIST";
        static final String COLUMN_NAME = "NAME";
        static final String COLUMN_DATE_TIME_CREATED = "DATE_TIME";
        static final String COLUMN_STATUS = "STATUS";
    }

    public static class TODOEntry implements BaseColumns {
        static final String TABLE_NAME = "TODO";
        static final String COLUMN_NAME = "NAME";
        static final String COLUMN_TODO_LIST_ID = "COLUMN_TODO_LIST_ID";
        static final String COLUMN_STATUS = "STATUS";
        static final String COLUMN_DATE_TIME_CREATED = "DATE_TIME";
    }

    public static final String CREATE_TABLE_TODO_LIST = "CREATE TABLE IF NOT EXISTS " + TODOListEntry.TABLE_NAME + "( "
            + TODOListEntry._ID + " INTEGER PRIMARY KEY, "
            + TODOListEntry.COLUMN_NAME + " TEXT, "
            + TODOListEntry.COLUMN_DATE_TIME_CREATED + " INTEGER "
            + " )";
    public static final String CREATE_TABLE_TODO = "CREATE TABLE IF NOT EXISTS " + TODOEntry.TABLE_NAME + "( "
            + TODOEntry._ID + " INTEGER PRIMARY KEY, "
            + TODOEntry.COLUMN_TODO_LIST_ID + " INTEGER, "
            + TODOEntry.COLUMN_NAME + " TEXT, "
            + TODOEntry.COLUMN_STATUS + " INTEGER, "
            + TODOEntry.COLUMN_DATE_TIME_CREATED + " INTEGER "
            + " )";

    public static final String DELETE_TABLE_TODO = "DELETE TABLE IF EXISTS " + TODOEntry.TABLE_NAME;
    public static final String DELETE_TABLE_TODO_LIST = "DELETE TABLE IF EXISTS " + TODOListEntry.TABLE_NAME;
}
