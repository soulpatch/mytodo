package com.soulpatch.mytodo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.soulpatch.mytodo.datamodel.PendingTodo;
import com.soulpatch.mytodo.datamodel.ToDoObj;
import com.soulpatch.mytodo.datamodel.ToDoObjList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Database helper for storing ToDos in the database
 *
 * @author Akshay Viswanathan.
 */
public class DBHelper {
    private static DBHelper mDBHelper;
    private static SQLiteDatabaseHelper mSQLiteDatabaseHelper;

    public static DBHelper getInstance(final Context context) {
        if (mDBHelper == null) {
            mDBHelper = new DBHelper();
        }

        mSQLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
        return mDBHelper;
    }

    public void insertTodoList(final String todoListName) {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getWritableDatabase();
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MyTodoDBContract.TODOListEntry.COLUMN_NAME, todoListName);
        contentValues.put(MyTodoDBContract.TODOListEntry.COLUMN_DATE_TIME_CREATED, System.currentTimeMillis());
        database.insert(MyTodoDBContract.TODOListEntry.TABLE_NAME, null, contentValues);
    }

    public ArrayList<ToDoObjList> getAllTodoObjList() {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getReadableDatabase();
        final Cursor cursor = database.query(MyTodoDBContract.TODOListEntry.TABLE_NAME, null, null, null, null, null, null);
        final ArrayList<ToDoObjList> completeList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                final ToDoObjList curToDoObjList = new ToDoObjList();
                curToDoObjList.setId(cursor.getLong(cursor.getColumnIndex(MyTodoDBContract.TODOListEntry._ID)));
                curToDoObjList.setName(cursor.getString(cursor.getColumnIndex(MyTodoDBContract.TODOListEntry.COLUMN_NAME)));
                curToDoObjList.setDateCreated(cursor.getInt(cursor.getColumnIndex(MyTodoDBContract.TODOListEntry.COLUMN_DATE_TIME_CREATED)));
                final ArrayList<ToDoObj> curToDoList = getAllToDoObjForList(curToDoObjList.getId());
                int completeTaskCount = 0;
                for (final ToDoObj toDoObj : curToDoList) {
                    if (toDoObj.getStatus() == 1) {
                        completeTaskCount++;
                    }
                }
                curToDoObjList.setTotalCompletedTasks(completeTaskCount);
                curToDoObjList.setTotalTasks(curToDoList.size());
                completeList.add(curToDoObjList);
            }
            cursor.close();
        }

        return completeList;
    }

    public void deleteTodoListForID(final long id) {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getReadableDatabase();
        final String selection = MyTodoDBContract.TODOListEntry._ID + " = ?";
        final String[] selectionArgs = {String.valueOf(id)};
        database.delete(MyTodoDBContract.TODOListEntry.TABLE_NAME, selection, selectionArgs);
    }

    public void insertTodo(final String todoName, final long todoListId) {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getWritableDatabase();
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MyTodoDBContract.TODOEntry.COLUMN_TODO_LIST_ID, todoListId);
        contentValues.put(MyTodoDBContract.TODOEntry.COLUMN_NAME, todoName);
        contentValues.put(MyTodoDBContract.TODOEntry.COLUMN_STATUS, 0);
        contentValues.put(MyTodoDBContract.TODOEntry.COLUMN_DATE_TIME_CREATED, System.currentTimeMillis());
        database.insert(MyTodoDBContract.TODOEntry.TABLE_NAME, null, contentValues);
    }

    public void updateTodo(final ToDoObj toDoObj) {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getWritableDatabase();
        final ContentValues contentValues = new ContentValues();
        contentValues.put(MyTodoDBContract.TODOEntry.COLUMN_STATUS, toDoObj.getStatus());
        final String selection = MyTodoDBContract.TODOEntry._ID + " = ? ";
        final String[] selectionArgs = new String[]{String.valueOf(toDoObj.getId())};
        database.update(MyTodoDBContract.TODOEntry.TABLE_NAME, contentValues, selection, selectionArgs);
    }

    public ArrayList<PendingTodo> getAllPendingToDo() {
        final HashMap<String, ArrayList<ToDoObj>> hashMap = new HashMap<>();
        final ArrayList<PendingTodo> pendingTodoList = new ArrayList<>();
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getReadableDatabase();
        final String selection = MyTodoDBContract.TODOEntry.COLUMN_STATUS + " = ? ";
        final String[] selectionArgs = {"0"};
        final Cursor cursor = database.query(MyTodoDBContract.TODOEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                final ToDoObj toDoObj = new ToDoObj();
                toDoObj.setId(cursor.getLong(cursor.getColumnIndex(MyTodoDBContract.TODOEntry._ID)));
                toDoObj.setTodoListID(cursor.getLong(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_TODO_LIST_ID)));
                toDoObj.setName(cursor.getString(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_NAME)));
                toDoObj.setStatus(cursor.getInt(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_STATUS)));
                toDoObj.setTimeCreated(cursor.getLong(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_DATE_TIME_CREATED)));
                final String todoListName = getTodoListObjNameForID(toDoObj.getTodoListID());
                final ArrayList<ToDoObj> toDoObjList = hashMap.containsKey(todoListName) ? hashMap.get(todoListName) : new ArrayList<ToDoObj>();
                toDoObjList.add(toDoObj);
                hashMap.put(todoListName, toDoObjList);
            }
            cursor.close();
        }

        for (final Map.Entry entry : hashMap.entrySet()) {
            final PendingTodo pendingTodo = new PendingTodo();
            pendingTodo.setTodoListName((String) entry.getKey());
            //noinspection unchecked
            pendingTodo.setToDoObjList((ArrayList<ToDoObj>) entry.getValue());
            pendingTodoList.add(pendingTodo);
        }
        return pendingTodoList;
    }

    public ArrayList<ToDoObj> getAllToDoObjForList(final long toDoListId) {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getReadableDatabase();
        final String selection = MyTodoDBContract.TODOEntry.COLUMN_TODO_LIST_ID + " = ?";
        final String[] selectionArgs = {String.valueOf(toDoListId)};
        final Cursor cursor = database.query(MyTodoDBContract.TODOEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        final ArrayList<ToDoObj> toDoObjs = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                final ToDoObj toDoObj = new ToDoObj();
                toDoObj.setId(cursor.getLong(cursor.getColumnIndex(MyTodoDBContract.TODOEntry._ID)));
                toDoObj.setTodoListID(cursor.getLong(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_TODO_LIST_ID)));
                toDoObj.setName(cursor.getString(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_NAME)));
                toDoObj.setStatus(cursor.getInt(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_STATUS)));
                toDoObj.setTimeCreated(cursor.getLong(cursor.getColumnIndex(MyTodoDBContract.TODOEntry.COLUMN_DATE_TIME_CREATED)));
                toDoObjs.add(toDoObj);
            }
            cursor.close();
        }

        return toDoObjs;
    }

    public void deleteTodoForID(final long id) {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getReadableDatabase();
        final String selection = MyTodoDBContract.TODOListEntry._ID + " = ?";
        final String[] selectionArgs = {String.valueOf(id)};
        database.delete(MyTodoDBContract.TODOEntry.TABLE_NAME, selection, selectionArgs);
    }

    private String getTodoListObjNameForID(final long id) {
        final SQLiteDatabase database = mSQLiteDatabaseHelper.getReadableDatabase();
        final String selection = MyTodoDBContract.TODOListEntry._ID + " = ?";
        final String[] selectionArgs = {String.valueOf(id)};
        final Cursor cursor = database.query(MyTodoDBContract.TODOListEntry.TABLE_NAME, null, selection, selectionArgs, null, null, null);
        String todoListName = null;
        if (cursor.moveToNext()) {
            todoListName = (cursor.getString(cursor.getColumnIndex(MyTodoDBContract.TODOListEntry.COLUMN_NAME)));
        }
        cursor.close();
        return todoListName;
    }
}
