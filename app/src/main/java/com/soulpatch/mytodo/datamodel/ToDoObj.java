package com.soulpatch.mytodo.datamodel;

import java.io.Serializable;

/**
 * Object that represents a to-do item
 *
 * @author Akshay Viswanathan
 */
public class ToDoObj implements Serializable {
    private long id;
    private long todoListID;
    private String name;
    private int status;
    private long timeCreated;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(final long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public long getTodoListID() {
        return todoListID;
    }

    public void setTodoListID(final long todoListID) {
        this.todoListID = todoListID;
    }
}