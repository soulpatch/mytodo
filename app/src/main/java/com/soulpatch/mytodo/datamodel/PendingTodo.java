package com.soulpatch.mytodo.datamodel;

import java.util.ArrayList;

/**
 * Object for displaying the Pending TodoObj list
 *
 * @author Akshay Viswanathan
 */
public class PendingTodo {
    private String todoListName;
    private ArrayList<ToDoObj> toDoObjList;

    public String getTodoListName() {
        return todoListName;
    }

    public void setTodoListName(final String todoListName) {
        this.todoListName = todoListName;
    }

    public ArrayList<ToDoObj> getToDoObjList() {
        return toDoObjList;
    }

    public void setToDoObjList(final ArrayList<ToDoObj> toDoObjList) {
        this.toDoObjList = toDoObjList;
    }
}
