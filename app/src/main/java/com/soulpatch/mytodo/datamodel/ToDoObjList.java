package com.soulpatch.mytodo.datamodel;

import java.io.Serializable;

/**
 * Object representing a list of {@link ToDoObj}
 *
 * @author Akshay Viswanathan
 */
public class ToDoObjList implements Serializable {
    private long id;
    private String name;
    private int dateCreated;
    private int totalCompletedTasks;
    private int totalTasks;
    private int status;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(final int dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getTotalCompletedTasks() {
        return totalCompletedTasks;
    }

    public void setTotalCompletedTasks(final int totalCompletedTasks) {
        this.totalCompletedTasks = totalCompletedTasks;
    }

    public int getTotalTasks() {
        return totalTasks;
    }

    public void setTotalTasks(final int totalTasks) {
        this.totalTasks = totalTasks;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }
}
