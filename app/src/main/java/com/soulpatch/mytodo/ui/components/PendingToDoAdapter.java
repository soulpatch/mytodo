package com.soulpatch.mytodo.ui.components;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.database.DBHelper;
import com.soulpatch.mytodo.datamodel.PendingTodo;
import com.soulpatch.mytodo.datamodel.ToDoObj;

import java.util.ArrayList;

/**
 * Adapter to show the list of items on the Pending Todos fragment
 *
 * @author Akshay Viswanathan
 */
public class PendingToDoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<PendingTodo> mValues;
    private final Context mContext;

    public PendingToDoAdapter(final ArrayList<PendingTodo> values, final Context context) {
        mValues = values;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_todo_view, parent, false);
        return new TextViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final PendingTodo pendingTodo = mValues.get(position);
        final TextViewHolder textViewHolder = (TextViewHolder) holder;
        textViewHolder.title.setText(pendingTodo.getTodoListName());
        final TodoObjAdapter adapter = new TodoObjAdapter(pendingTodo.getToDoObjList(), textViewHolder.pendingTodoList.getContext(), new TodoObjDeleteListener() {
            @Override
            public void onItemClicked(final ToDoObj toDoObj) {
                DBHelper.getInstance(mContext).deleteTodoForID(toDoObj.getId());
                textViewHolder.pendingTodoList.getAdapter().notifyDataSetChanged();
            }
        });
        textViewHolder.pendingTodoList.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    private class TextViewHolder extends RecyclerView.ViewHolder {
        final TextView title;
        final RecyclerView pendingTodoList;

        TextViewHolder(final View rootView) {
            super(rootView);
            title = rootView.findViewById(R.id.todo_list_name);
            pendingTodoList = rootView.findViewById(R.id.pending_todo_list);

            if (title == null || pendingTodoList == null) {
                throw new IllegalArgumentException("Cannot inflate all the views in the View Holder");
            }
            pendingTodoList.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        }
    }
}
