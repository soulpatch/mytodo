package com.soulpatch.mytodo.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.database.DBHelper;
import com.soulpatch.mytodo.datamodel.PendingTodo;
import com.soulpatch.mytodo.ui.components.PendingToDoAdapter;

import java.util.ArrayList;

/**
 * Fragment that shows the list of Pending list
 *
 * @author Akshay Viswanathan
 */
public class PendingTodoListFragment extends Fragment {

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.pending_todo_header);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_pending_todo_list, container, false);
        final ArrayList<PendingTodo> pendingTodoList = DBHelper.getInstance(getActivity()).getAllPendingToDo();
        final RecyclerView recyclerView = linearLayout.findViewById(R.id.pending_todo_list);
        final TextView noPendingToDosMessage = linearLayout.findViewById(R.id.no_pending_todos_message);
        final ImageView noPendingTodoImage = linearLayout.findViewById(R.id.no_pending_todos_image);

        if (pendingTodoList.isEmpty()) {
            noPendingToDosMessage.setVisibility(View.VISIBLE);
            noPendingTodoImage.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            final PendingToDoAdapter adapter = new PendingToDoAdapter(pendingTodoList, getActivity());
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(adapter);
            noPendingToDosMessage.setVisibility(View.GONE);
            noPendingTodoImage.setVisibility(View.GONE);
        }
        return linearLayout;
    }
}
