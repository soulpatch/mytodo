package com.soulpatch.mytodo.ui.components;

import com.soulpatch.mytodo.datamodel.ToDoObj;

/**
 * Interface for specifying a delete listener for an adapter used in a RecyclerView
 *
 * @author Akshay Viswanathan
 */
public interface TodoObjDeleteListener {
    void onItemClicked(ToDoObj toDoObj);
}
