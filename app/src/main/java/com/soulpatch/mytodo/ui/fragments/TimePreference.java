package com.soulpatch.mytodo.ui.fragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.preference.DialogPreference;
import android.util.AttributeSet;

import com.soulpatch.mytodo.R;

/**
 * {@link DialogPreference} for setting the time of the reminder alarms
 *
 * @author Akshay Viswanathan
 */
class TimePreference extends DialogPreference {
    private static final String TAG = TimePreference.class.getName();
    private int mTime;

    public TimePreference(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public TimePreference(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TimePreference(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public TimePreference(final Context context) {
        super(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClick() {
        getPreferenceManager().showDialog(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object onGetDefaultValue(final TypedArray typedArray, final int index) {
        return typedArray.getInt(index, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onSetInitialValue(final boolean restorePersistedValue, final Object defaultValue) {
        setTime(restorePersistedValue ? getPersistedInt(mTime) : (Integer) defaultValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDialogLayoutResource() {
        return R.layout.pref_dialog_time;
    }

    public int getTime() {
        return mTime;
    }

    public void setTime(final int time) {
        mTime = time;
        persistInt(time);
    }
}