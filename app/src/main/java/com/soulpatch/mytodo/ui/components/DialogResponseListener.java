package com.soulpatch.mytodo.ui.components;

import android.support.v7.app.AlertDialog;
import android.widget.EditText;

/**
 * Listener interface that listens to the positive and negative button clicks on an AlertDialog
 *
 * @author Akshay Viswanathan
 */
public interface DialogResponseListener {
    void onPositiveClickListener(AlertDialog alertDialog, EditText editText);

    void onNegativeClickListener(AlertDialog alertDialog, EditText editText);
}
