package com.soulpatch.mytodo.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.database.DBHelper;
import com.soulpatch.mytodo.datamodel.ToDoObjList;
import com.soulpatch.mytodo.ui.components.AdapterClickListener;
import com.soulpatch.mytodo.ui.components.DialogResponseListener;
import com.soulpatch.mytodo.ui.components.ToDoObjListDeleteListener;
import com.soulpatch.mytodo.ui.components.TodoObjListAdapter;
import com.soulpatch.mytodo.utils.Utils;

import java.util.ArrayList;

/**
 * The main fragment that loads when the app launches
 *
 * @author Akshay Viswanathan
 */
public class MainFragment extends Fragment {
    private static final String TAG = MainFragment.class.getSimpleName();
    private final ArrayList<ToDoObjList> mValues = new ArrayList<>();
    private RecyclerView mRecyclerView;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        if (getArguments() != null && getArguments().getString(Utils.BUNDLE_ARG_ALARM) != null) {
            //We know this has been called by the alarm and not by the user's launching of the app
            final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new PendingTodoListFragment()).addToBackStack(null).commit();
        }

        super.onCreate(savedInstanceState);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_main, container, false);
        mRecyclerView = relativeLayout.findViewById(R.id.list_layout);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        final TodoObjListAdapter adapter = new TodoObjListAdapter(getActivity(), mValues, new AdapterClickListener() {
            @Override
            public void onItemClicked(final ToDoObjList toDoObjList) {
                final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, TodoListDetailFragment.getInstance(toDoObjList)).addToBackStack(null).commit();
            }
        }, new ToDoObjListDeleteListener() {
            @Override
            public void onItemDelete(final ToDoObjList toDoObjList) {
                DBHelper.getInstance(getActivity()).deleteTodoListForID(toDoObjList.getId());
                mValues.remove(toDoObjList);
                mRecyclerView.getAdapter().notifyDataSetChanged();
            }
        });
        mRecyclerView.setAdapter(adapter);

        final Button addList = relativeLayout.findViewById(R.id.add_list_button);
        addList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Utils.showAddEditDialog((AppCompatActivity) getActivity(), getString(R.string.add_new_todo_list), new DialogResponseListener() {
                    @Override
                    public void onPositiveClickListener(final AlertDialog alertDialog, final EditText editText) {
                        final String newTodoName = editText.getText().toString();
                        if (!TextUtils.isEmpty(newTodoName)) {
                            DBHelper.getInstance(getActivity()).insertTodoList(newTodoName);
                            refreshList();
                            alertDialog.dismiss();
                        }
                    }

                    @Override
                    public void onNegativeClickListener(final AlertDialog alertDialog, final EditText editText) {
                        alertDialog.dismiss();
                    }
                });
            }
        });

        return relativeLayout;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onResume() {
        super.onResume();

        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.app_name);
        }

        refreshList();
    }

    private void refreshList() {
        mValues.clear();
        mValues.addAll(DBHelper.getInstance(getActivity()).getAllTodoObjList());
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }
}
