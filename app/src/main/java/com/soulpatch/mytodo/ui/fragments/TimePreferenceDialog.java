package com.soulpatch.mytodo.ui.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.DialogPreference;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;

import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.utils.Utils;

import java.util.Calendar;


/**
 * The Dialog for the {@link TimePreference}
 *
 * @author Akshay Viswanathan
 */
public class TimePreferenceDialog extends PreferenceDialogFragmentCompat {
    private static final String TAG = TimePreferenceDialog.class.getName();
    private TimePicker mTimePicker;

    /**
     * Creates a new Instance of the TimePreferenceDialogFragment and stores the key of the
     * related Preference
     *
     * @param key The key of the related Preference
     * @return A new Instance of the TimePreferenceDialogFragment
     */
    public static TimePreferenceDialog newInstance(final String key) {
        final TimePreferenceDialog fragment = new TimePreferenceDialog();
        final Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);

        return fragment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);

        mTimePicker = view.findViewById(R.id.edit);

        if (mTimePicker == null) {
            throw new IllegalStateException("Dialog view must contain a TimePicker with id 'edit'");
        }

        // Get the time from the related Preference
        Integer alarmTime = null;
        final DialogPreference preference = getPreference();
        if (preference instanceof TimePreference) {
            alarmTime = ((TimePreference) preference).getTime();
            Log.i(TAG, "Getting time " + alarmTime);
        }

        // Set the time to the TimePicker
        if (alarmTime != null) {
            final int hours = alarmTime / 60;
            final int minutes = alarmTime % 60;
            final boolean is24hour = DateFormat.is24HourFormat(getContext());

            mTimePicker.setIs24HourView(is24hour);
            mTimePicker.setCurrentHour(hours);
            mTimePicker.setCurrentMinute(minutes);
            Log.i(TAG, "Repeating alarm set " + Utils.mSimpleTimeFormat.format(alarmTime));
        }
    }

    /**
     * Called when the Dialog is closed.
     *
     * @param positiveResult Whether the Dialog was accepted or canceled.
     */
    @Override
    public void onDialogClosed(final boolean positiveResult) {
        if (positiveResult) {
            Log.i(TAG, "onDialogClosed: positiveResult");
            // Get the current values from the TimePicker
            final int hours;
            final int minutes;
            if (Build.VERSION.SDK_INT >= 23) {
                hours = mTimePicker.getHour();
                minutes = mTimePicker.getMinute();
            } else {
                hours = mTimePicker.getCurrentHour();
                minutes = mTimePicker.getCurrentMinute();
            }

            // Generate value to save
            final int alarmTime = (hours * 60) + minutes;

            // Save the value
            final DialogPreference preference = getPreference();
            if (preference instanceof TimePreference) {
                final TimePreference timePreference = ((TimePreference) preference);
                // This allows the client to ignore the user value.
                if (timePreference.callChangeListener(alarmTime)) {
                    // Save the value
                    timePreference.setTime(alarmTime);
                    final Calendar calendar = Calendar.getInstance();
                    calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), hours, minutes);
                    Utils.setAlarm((AppCompatActivity) getActivity(), hours, minutes);
                    Log.i(TAG, "Repeating alarm set " + Utils.mSimpleTimeFormat.format(calendar.getTimeInMillis()));
                }
            }
        }
    }
}
