package com.soulpatch.mytodo.ui.components;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.datamodel.ToDoObjList;

import java.util.ArrayList;

/**
 * Adapter for a list of {@link ToDoObjList}
 *
 * @author Akshay Viswanathan
 */
public class TodoObjListAdapter extends RecyclerView.Adapter<TodoObjListAdapter.ViewHolder> {
    private final ArrayList<ToDoObjList> mValues;
    private final AdapterClickListener mAdapterClickListener;
    private final ToDoObjListDeleteListener mToDoObjListDeleteListener;
    private final Context mContext;

    public TodoObjListAdapter(final Context context, final ArrayList<ToDoObjList> values, final AdapterClickListener adapterClickListener, ToDoObjListDeleteListener toDoObjListDeleteListener) {
        mValues = values;
        mAdapterClickListener = adapterClickListener;
        mToDoObjListDeleteListener = toDoObjListDeleteListener;
        mContext = context;
    }

    @Override
    public TodoObjListAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_todo_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TodoObjListAdapter.ViewHolder holder, final int position) {
        final ToDoObjList toDoObjList = mValues.get(position);
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mAdapterClickListener.onItemClicked(toDoObjList);
            }
        });
        holder.todoListTitle.setText(toDoObjList.getName());
        strikeThroughText(holder.todoListTitle, toDoObjList.getTotalTasks() > 0 && toDoObjList.getTotalCompletedTasks() == toDoObjList.getTotalTasks());
        holder.todoCount.setText(mContext.getString(R.string.count_display, toDoObjList.getTotalCompletedTasks(), toDoObjList.getTotalTasks()));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mToDoObjListDeleteListener.onItemDelete(toDoObjList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final CardView mRootView;
        final TextView todoListTitle;
        final TextView todoCount;
        final ImageView delete;

        ViewHolder(final View rootView) {
            super(rootView);
            mRootView = (CardView) rootView;
            todoListTitle = rootView.findViewById(R.id.todo_list_title);
            todoCount = rootView.findViewById(R.id.todo_list_task_count);
            delete = rootView.findViewById(R.id.todo_list_delete);

            if (todoListTitle == null || todoCount == null || delete == null) {
                throw new IllegalArgumentException("Could not inflate the ViewHolder");
            }
        }
    }

    private void strikeThroughText(final TextView textView, final boolean isChecked) {
        if (isChecked) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }
}
