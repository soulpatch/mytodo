package com.soulpatch.mytodo.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.database.DBHelper;
import com.soulpatch.mytodo.datamodel.ToDoObj;
import com.soulpatch.mytodo.datamodel.ToDoObjList;
import com.soulpatch.mytodo.ui.components.DialogResponseListener;
import com.soulpatch.mytodo.ui.components.TodoObjAdapter;
import com.soulpatch.mytodo.ui.components.TodoObjDeleteListener;
import com.soulpatch.mytodo.utils.Utils;

import java.util.ArrayList;

/**
 * Fragment that displays the list of {@link com.soulpatch.mytodo.datamodel.ToDoObj} that are part of the selected list
 *
 * @author Akshay Viswanathan
 */
public class TodoListDetailFragment extends Fragment {
    private static final String BUNDLE_ARG = "BUNDLE_ARG";
    private final ArrayList<ToDoObj> mValues = new ArrayList<>();
    private ToDoObjList mToDoObjList;
    private RecyclerView mRecyclerView;

    public static Fragment getInstance(final ToDoObjList toDoObjList) {
        final Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_ARG, toDoObjList);
        final TodoListDetailFragment fragment = new TodoListDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments == null || !arguments.containsKey(BUNDLE_ARG)) {
            throw new IllegalStateException("Cannot call this fragment without valid arguments");
        }

        mToDoObjList = (ToDoObjList) arguments.getSerializable(BUNDLE_ARG);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_todo_list, container, false);
        mRecyclerView = relativeLayout.findViewById(R.id.todo_list_layout);
        final TodoObjAdapter adapter = new TodoObjAdapter(mValues, getActivity(), new TodoObjDeleteListener() {
            @Override
            public void onItemClicked(final ToDoObj toDoObj) {
                DBHelper.getInstance(getActivity()).deleteTodoForID(toDoObj.getId());
                mValues.remove(toDoObj);
                mRecyclerView.getAdapter().notifyDataSetChanged();
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(adapter);

        final Button addList = relativeLayout.findViewById(R.id.add_todo_button);
        addList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Utils.showAddEditDialog((AppCompatActivity) getActivity(), getString(R.string.add_new_todo_title), new DialogResponseListener() {
                    @Override
                    public void onPositiveClickListener(final AlertDialog alertDialog, final EditText editText) {
                        final String newTodoName = editText.getText().toString();
                        if (!TextUtils.isEmpty(newTodoName)) {
                            DBHelper.getInstance(getActivity()).insertTodo(newTodoName, mToDoObjList.getId());
                            refreshList();
                            alertDialog.dismiss();
                        }
                    }

                    @Override
                    public void onNegativeClickListener(final AlertDialog alertDialog, final EditText editText) {
                        alertDialog.dismiss();
                    }
                });
            }
        });

        return relativeLayout;
    }

    @Override
    public void onResume() {
        super.onResume();

        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mToDoObjList.getName());
        }

        refreshList();
    }

    private void refreshList() {
        mValues.clear();
        mValues.addAll(DBHelper.getInstance(getActivity()).getAllToDoObjForList(mToDoObjList.getId()));
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }
}
