package com.soulpatch.mytodo.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.soulpatch.mytodo.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Fragment to show statistics to the user
 *
 * @author Akshay Viswanathan
 */
public class StatisticsFragment extends Fragment {
    private static final String TAG = StatisticsFragment.class.getName();

    /**
     * {@inheritDoc}
     */
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView");
        final GraphView graph = (GraphView) inflater.inflate(R.layout.statistics_fragment, container, false);
        final Calendar calendar = Calendar.getInstance();
        final Date d1 = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        final Date d2 = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        final Date d3 = calendar.getTime();
        final LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(d1, 1),
                new DataPoint(d2, 2),
                new DataPoint(d3, 3)
        });
        graph.addSeries(series);

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM", Locale.ENGLISH);
        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity(), simpleDateFormat));
        graph.getGridLabelRenderer().setNumHorizontalLabels(3); // only 4 because of the space

        // set manual x bounds to have nice steps
        graph.getViewport().setMinX(d1.getTime());
        graph.getViewport().setMaxX(d3.getTime());
        graph.getViewport().setXAxisBoundsManual(true);

        // as we use dates as labels, the human rounding to nice readable numbers
        // is not necessary
        graph.getGridLabelRenderer().setHumanRounding(false);
        return graph;
    }
}
