package com.soulpatch.mytodo.ui.components;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.database.DBHelper;
import com.soulpatch.mytodo.datamodel.ToDoObj;
import com.soulpatch.mytodo.utils.Utils;

import java.util.ArrayList;

/**
 * Adapter for a list of {@link ToDoObj}
 *
 * @author Akshay Viswanathan
 */
public class TodoObjAdapter extends RecyclerView.Adapter<TodoObjAdapter.ViewHolder> {
    private final ArrayList<ToDoObj> mValues;
    private final Context mContext;
    private final TodoObjDeleteListener mTodoObjDeleteListener;

    public TodoObjAdapter(final ArrayList<ToDoObj> values, final Context context, final TodoObjDeleteListener toDoListDeleteListener) {
        mValues = values;
        mContext = context;
        mTodoObjDeleteListener = toDoListDeleteListener;
    }

    @Override
    public TodoObjAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_todo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TodoObjAdapter.ViewHolder holder, final int position) {
        final ToDoObj toDoObj = mValues.get(position);
        holder.title.setText(toDoObj.getName());
        strikeThroughText(holder.title, toDoObj.getStatus() == 1);

        holder.checkBox.setChecked(toDoObj.getStatus() == 1);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                if (isChecked != (toDoObj.getStatus() == 1)) {
                    toDoObj.setStatus(isChecked ? 1 : 0);
                    strikeThroughText(holder.title, isChecked);
                    DBHelper.getInstance(mContext).updateTodo(toDoObj);
                }
            }
        });

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                holder.checkBox.setChecked(!holder.checkBox.isChecked());
            }
        });

        holder.title.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                Utils.showAddEditDialog((AppCompatActivity) mContext, "Edit Todo", new DialogResponseListener() {
                    @Override
                    public void onPositiveClickListener(final AlertDialog alertDialog, final EditText editText) {
                        final String todoName = editText.getText().toString();
                        toDoObj.setName(todoName);
                        mValues.remove(toDoObj);
                        DBHelper.getInstance(mContext).updateTodo(toDoObj);
                    }

                    @Override
                    public void onNegativeClickListener(final AlertDialog alertDialog, final EditText editText) {
                        alertDialog.dismiss();
                    }
                });
                return true;
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mTodoObjDeleteListener.onItemClicked(toDoObj);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final CheckBox checkBox;
        final TextView title;
        final ImageView delete;

        ViewHolder(final View rootView) {
            super(rootView);
            checkBox = rootView.findViewById(R.id.todo_checkbox);
            title = rootView.findViewById(R.id.todo_title);
            delete = rootView.findViewById(R.id.todo_delete);

            if (checkBox == null || title == null) {
                throw new IllegalArgumentException("Cannot inflate all the views in the View Holder");
            }
        }
    }

    private void strikeThroughText(final TextView textView, final boolean isChecked) {
        if (isChecked) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            textView.setPaintFlags(textView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }
}
