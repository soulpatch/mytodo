package com.soulpatch.mytodo.ui.components;

import com.soulpatch.mytodo.datamodel.ToDoObjList;

/**
 * Interface for specifying a click listener for an adapter used in a RecyclerView
 *
 * @author Akshay Viswanathan
 */
public interface AdapterClickListener {
    void onItemClicked(ToDoObjList toDoObjList);
}
