package com.soulpatch.mytodo.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.soulpatch.mytodo.MainActivity;
import com.soulpatch.mytodo.R;
import com.soulpatch.mytodo.ui.components.DialogResponseListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Class for having util methods for application wide usage
 *
 * @author Akshay Viswanathan
 */
public final class Utils {
    public static final String TAG = Utils.class.getSimpleName();
    public static final String BUNDLE_ARG_ALARM = "BUNDLE_ARG_ALARM";
    private static final int REQUEST_CODE = 0;
    public static final SimpleDateFormat mSimpleTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.ENGLISH);

    public static void setAlarm(final AppCompatActivity activity, final int hour, final int minute) {
        Log.i(TAG, "setAlarm called");
        final Intent intent = new Intent(activity, MainActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        final Bundle arguments = new Bundle();
        arguments.putString(BUNDLE_ARG_ALARM, "Alarm");

        final boolean alarmUp = (PendingIntent.getBroadcast(activity, 0, intent, PendingIntent.FLAG_NO_CREATE) != null);
        if (alarmUp) {
            Log.i("myTag", "Alarm is already active");
        }

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        final AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        final Calendar calendar = Calendar.getInstance();
        //Set to the nearest 9 pm to the current time
        if (calendar.get(Calendar.HOUR_OF_DAY) > hour) {
            calendar.add(Calendar.DATE, 1);
        }

        if (calendar.get(Calendar.HOUR_OF_DAY) == hour && calendar.get(Calendar.MINUTE) > minute) {
            calendar.add(Calendar.DATE, 1);
        }

        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), hour, minute, 0);

        if (alarmManager != null) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
            Log.i(TAG, "Repeating alarm set " + mSimpleTimeFormat.format(calendar.getTimeInMillis()));
        }
    }

    public static void showAddEditDialog(final AppCompatActivity context, final String title, final DialogResponseListener dialogResponseListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LinearLayout linearLayout = (LinearLayout) context.getLayoutInflater().inflate(R.layout.dialog_add_edit, null, false);
        builder.setView(linearLayout);
        builder.setTitle(title);
        builder.setPositiveButton(context.getString(android.R.string.ok), null);
        builder.setNegativeButton(context.getString(android.R.string.cancel), null);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        //Doing it this way instead of in setPositiveButton since this will be executed after the view is created, at which point we have the edit text ready
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final EditText editText = linearLayout.findViewById(R.id.dialog_add_todo_name);
                dialogResponseListener.onPositiveClickListener(alertDialog, editText);
            }
        });
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final EditText editText = linearLayout.findViewById(R.id.dialog_add_todo_name);
                dialogResponseListener.onNegativeClickListener(alertDialog, editText);
            }
        });
    }
}
